
---

# Jenkins Interface Tour

---

# Jenkins Interface Tour

Here is a looks of fresh jenkins installation from our lab setup:

<img src="99_misc/.img/first_login.png" alt="first login" style="float:center;width:600px;">

---
# Right Menu
<img src="99_misc/.img/classic-ui-right.png" alt="right" style="float:center;width:600px;">

---
# Left Menu
<img src="99_misc/.img/classic-ui-left-column-on-item.png" alt="left" style="float:center;width:300px;">

---
# New Item
<img src="99_misc/.img/classic-ui-newitem.png" alt="new-item" style="float:center;width:600px;">

---
# Manage Jenkins

<img src="99_misc/.img/manage.png" alt="manage" style="float:center;width:600px;">

---

# Jenkins Interface Tour (cont.)

<img src="99_misc/.img/manage.png" alt="manage" style="float:center;width:600px;">
